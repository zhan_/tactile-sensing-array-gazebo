#include "SensorPlugin.hh"

using namespace gazebo;
GZ_REGISTER_SENSOR_PLUGIN(GazeboRosSensor)

struct CompareMsg {
	bool operator()(const gazebo_ros_sensor_plugin::TexelData& _a, const gazebo_ros_sensor_plugin::TexelData& _b)
	{
		      return _a.texelId < _b.texelId;
	}
};

struct Max_elemMsg {
	bool operator()(const gazebo_ros_sensor_plugin::TexelData& _a, const gazebo_ros_sensor_plugin::TexelData& _b)
	{
		      return _a.force < _b.force;
	}
};



//Constructor
GazeboRosSensor::GazeboRosSensor() : SensorPlugin()
{
}

//Destructor
GazeboRosSensor::~GazeboRosSensor()
{
	this->parentSensor_->SetActive(false);
	for (int i=0;i<this->collisionCount_;i++)
	{
	delete this->fi_[i];
	}
  this->rosnode_->shutdown();
  this->callback_queue_thread_.join();
  delete this->rosnode_;
}

//load Parameters and initializes ros nodes
void GazeboRosSensor::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
	//read the values of  SDF
	GazeboRosSensor::loadParameters(_sensor,_sdf);
	//Get the collision names of each texel
	GazeboRosSensor::GetCollisionNamesToArea();
	//init sensor and ros nodes
	GazeboRosSensor::initSensor();

	  if (this->collisionCount_ != this->rows_ * this->columns_)
  {
    ROS_FATAL_STREAM("The sensor need the correct number of collision = number of texels = rows*columns");
    return;
  }
}

/////////////////////////////////////////////////
void GazeboRosSensor::OnUpdate()
{

	GazeboRosSensor::updateSensorMsg();
	GazeboRosSensor::convoluteSensorMsg();
 	GazeboRosSensor::updateImageForce();
	GazeboRosSensor::updateImageSensor();
	
	if (this->hz_count_ >= this->sample_rate_/this->tactile_rate_)
	{
		this->contact_pub_.publish(this->sensorMsg_);
		this->image_sensor_pub_.publish(this->sensorImage_);
		this->image_force_pub_.publish(this->forceImage_);
		this->hz_count_=1;
	}
	else {this->hz_count_++;}
}


void GazeboRosSensor::updateSensorMsg()
{
 	this->sensorMsg_.sensorData.clear();
// For each collision attached to this sensor
  boost::unordered_map<std::string, double>::iterator iter;
 	unsigned int countcont=0;  
  for (iter = this->collisionNamesToArea_.begin();
       iter != this->collisionNamesToArea_.end(); ++iter)
  {
    double normalForceSum = 0, normalForce;
    // Get the contacts sorted by collision element.
    std::map<std::string, gazebo::physics::Contact> contacts;
    std::map<std::string, gazebo::physics::Contact>::iterator iter2;
    contacts = this->parentSensor_->GetContacts(iter->first);
   	
    for (iter2 = contacts.begin(); iter2 != contacts.end(); ++iter2)
    {
      for (int i = 0; i < iter2->second.count; ++i)
      {
        
        normalForce = iter2->second.normals[i].x *
                      iter2->second.wrench[i].body1Force.x +
                      iter2->second.normals[i].y *
                      iter2->second.wrench[i].body1Force.y +
                      iter2->second.normals[i].z *
                      iter2->second.wrench[i].body1Force.z;   
                      
        normalForceSum += normalForce;
      }
    	
    }
    
    //bref: extract the Id of the texel
    std::string id="";
   	std::string collisionName=iter->first.substr(iter->first.rfind("::") + 2);
    
		 for (unsigned int i=0; i < collisionName.size(); i++)
		  {
		      if (isdigit(collisionName[i]))
		      {
		          for (unsigned int a=i; a<collisionName.size(); a++)
		          {
		              id += collisionName[a];               
		          }
		        break;
		      }
		      
		  }
		  if (!isdigit(id[0])) {
		  ROS_ERROR_STREAM(" Collision "<<collisionName<<" need to contain a int for the texel_id");}    
		  
    gazebo_ros_sensor_plugin::TexelData texelMsg;
    if (normalForceSum < 0 )
    {
	    texelMsg.texelId = boost::lexical_cast<unsigned int>(id);
		  texelMsg.force = -normalForceSum;
		  texelMsg.pressure = -normalForceSum / iter->second;
    }
    else 
    {
			texelMsg.texelId = boost::lexical_cast<unsigned int>(id);
		  texelMsg.force = normalForceSum;
		  texelMsg.pressure = normalForceSum / iter->second; 
    }

		this->sensorMsg_.sensorData.push_back(texelMsg); 

  }
  
  std::sort(this->sensorMsg_.sensorData.begin(), this->sensorMsg_.sensorData.end(), CompareMsg());

	    
////////////////////////////  filtering  ////////////////////////////  
			
    double *forceData;

	//std::cout<<this->sensorMsg_.sensorData[38].force<<",";    		
	  //std::cout<<this->sensorMsg_.sensorData[13].force<<",";    		
		for (int i = 0; i< this->sensorMsg_.sensorData.size(); ++i)
    {
	    forceData=&(this->sensorMsg_.sensorData[i].force);
		
      if (*(forceData)==0)
	    { 
	    bufferForce_[i]++;
	    }
	    else {bufferForce_[i]=0;}
	    if (bufferForce_[i]>=100)
	    {
	    //std::cout<<"reset texel : "<<i<<"\n";
	    fi_[i]->reset();
	    }

    	//applied the filter with one simple
    	fi_[i]->process (1, &forceData);
    	this->sensorMsg_.sensorData[i].force = *(forceData)*0.00003;//0.000011561

		}		 
 		//std::cout<<this->sensorMsg_.sensorData[38].force<<",";    		
		//std::cout<<this->sensorMsg_.sensorData[13].force<<";"; 
 	     
this->sensorMsg_.nameSensor = this->ModelName_;
this->sensorMsg_.header.seq = this->seq_;
this->sensorMsg_.header.stamp = ros::Time::now();
this->sensorMsg_.header.frame_id = "0";
  	
this->seq_++;

}




void GazeboRosSensor::convoluteSensorMsg()
{

double forcedata[this->rows_ * this->columns_];
for (int i = 0; i< sensorMsg_.sensorData.size(); ++i)
    {
    forcedata[i] = sensorMsg_.sensorData[i].force;
    }
	cv::Mat	M = cv::Mat(this->rows_,this->columns_,CV_64F,forcedata);	

    cv::Mat kernelx = cv::getGaussianKernel(this->ksizex_,this->sigmax_, CV_64F );
    cv::Mat kernely = cv::getGaussianKernel(this->ksizey_,this->sigmay_, CV_64F );
    cv::Mat kernel = kernely * kernelx.t();
    
    int centerx = kernel.cols/2;
    int centery = kernel.rows/2;

    double a = kernel.at<double>(cv::Point(centerx,centery));
    kernel=kernel*(1/a); //gaussien unitaire

    /// Applied gaussien filter
    cv::filter2D(M, M, M.depth() , kernel, cv::Point( -1, -1 ), 0, cv::BORDER_DEFAULT );
    
std::vector<double> array;
array.assign((double*)M.datastart, (double*)M.dataend);

for (int i = 0; i< sensorMsg_.sensorData.size(); ++i)
    {
   		
      float tempForce = array[i];
			if (tempForce <= this->rangeMin_)
			{ 
				sensorMsg_.sensorData[i].sensorOut = this->satMin_;
			}
			else if (tempForce >= this->rangeMax_)
				{ 
				sensorMsg_.sensorData[i].sensorOut = this->satMax_;
				}
				else {
						//linear equation
						sensorMsg_.sensorData[i].sensorOut = tempForce*this->liniaritySensibility_ + this->liniarityOfset_;
						}
		 }
}


void GazeboRosSensor::updateImageForce()
{
  this->forceImage_.data.clear();
  this->forceImage_.header.stamp = this->sensorMsg_.header.stamp;
  this->forceImage_.header.frame_id = this->sensorMsg_.header.frame_id;

  this->forceImage_.height = this->rows_;
  this->forceImage_.width = this->columns_;
  this->forceImage_.step = this->columns_*2; 
  this->forceImage_.encoding="mono16";//8UC1; 
  this->forceImage_.is_bigendian=0; 


  std::vector<gazebo_ros_sensor_plugin::TexelData>::iterator ItmaxForce = std::max_element(this->sensorMsg_.sensorData.begin(), this->sensorMsg_.sensorData.end(), Max_elemMsg());
  gazebo_ros_sensor_plugin::TexelData maxForce = this->sensorMsg_.sensorData[std::distance(this->sensorMsg_.sensorData.begin(), ItmaxForce)];


	for (int i = 0; i< this->sensorMsg_.sensorData.size(); ++i)
	{
			if (maxForce.force > 0)
			{
 	     long int  temp = (this->sensorMsg_.sensorData[i].force * 65535) / maxForce.force;
 	     this->forceImage_.data.push_back(temp/256); //octet Most significant bit: , testvaleur / 256
	     this->forceImage_.data.push_back(temp%256); //octet least significant byte: , testvaleur % 256
	 		}
	 		else
	 		{	
	 		 this->forceImage_.data.push_back(0);
  	 	 this->forceImage_.data.push_back(0);
  	 	}
   }
}



void GazeboRosSensor::updateImageSensor()
{
	this->sensorImage_.data.clear();
  this->sensorImage_.header.stamp = this->sensorMsg_.header.stamp;
  this->sensorImage_.header.frame_id = this->sensorMsg_.header.frame_id;
  
  this->sensorImage_.height = this->rows_;
  this->sensorImage_.width = this->columns_;
  this->sensorImage_.step = this->columns_; 
  this->sensorImage_.encoding="8UC1";//"mono16";
  this->sensorImage_.is_bigendian=0; 
  unsigned int MAX_VAL =this->satMax_; //4096

	for (int i = 0; i< this->sensorMsg_.sensorData.size(); ++i)
	{
	 float val = this->sensorMsg_.sensorData[i].sensorOut;//*1.5;
	 this->sensorImage_.data.push_back(int(255-(255*(float(float(val)/float(MAX_VAL))))));
  }
}


void GazeboRosSensor::initSensor()
{
	  // Make sure the ROS node for Gazebo has already been initialized
  if (!ros::isInitialized())
  {
    ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
      << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
    return;
  }


  // Connect to the sensor update event.
  this->updateConnection_ = this->parentSensor_->ConnectUpdated(
      boost::bind(&GazeboRosSensor::OnUpdate, this));

  // Make sure the parent sensor is active.
  this->parentSensor_->SetActive(true);
  
  //created ROS node
  this->rosnode_ = new ros::NodeHandle(this->robot_namespace_);
  
	// Create publisher for tactile messages
	this->contact_pub_ = this->rosnode_->advertise<gazebo_ros_sensor_plugin::SensorData>(
    std::string(this->texel_topic_name_), 1);
    
  this->image_force_pub_ = this->rosnode_->advertise<sensor_msgs::Image>(
    std::string(this->image_force_topic_name_), 1);  
    
  this->image_sensor_pub_ = this->rosnode_->advertise<sensor_msgs::Image>(
    std::string(this->image_sensor_topic_name_), 1);  
        
  // start custom queue for contact texel
  this->callback_queue_thread_ = boost::thread(
      boost::bind(&GazeboRosSensor::ContactQueueThread, this));
      
	this->seq_=0;
	this->hz_count_=1;
 	this->sensorMsg_.sensorData.clear();
 	
 	std::vector<double> tableau(this->collisionCount_, 0);//tableau ou tout les elements vaaux 0
 	this->bufferForce_ = tableau;
 	
 	//init the filter
  physics::WorldPtr world = physics::get_world(this->worldName_);
	physics::PhysicsEnginePtr physicsEngine = world->GetPhysicsEngine();
 	double updateRate = physicsEngine->GetRealTimeUpdateRate();

  
  Dsp::Params params;
  if (this->sample_rate_==0 || this->sample_rate_>updateRate)
  {
  this->sample_rate_=updateRate;
  }
	params[0] = this->sample_rate_;     // sample rate
  params[1] = this->orderFilter_;         // order
  params[2] = this->cutFrequency_;     // fc

  for (int i=0;i<this->collisionCount_;i++)
  {
  Dsp::Filter* filter = new Dsp::FilterDesign <Dsp::Butterworth::Design::LowPass <10>, 1 >;
  filter->setParams (params);
  fi_.push_back(filter);
  }


}




void GazeboRosSensor::loadParameters(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
  // Get the parent sensor.
  this->parentSensor_ = boost::dynamic_pointer_cast<sensors::ContactSensor>(_sensor);

  // Make sure the parent sensor is valid.
  if (!this->parentSensor_)
  {
    ROS_ERROR("GazeboRosSensor plugin requires a ContactSensor.");
    return;
  }


//rangeMax ==>saturation
	if (!_sdf->HasElement("rangeMax"))
  {
    ROS_DEBUG("Missing paramete <rangeMax> in GazeboRosTexel, defaults to 2^16");
    this->rangeMax_= 65536;
  }
  else
    this->rangeMax_ = _sdf->GetElement("rangeMax")->Get<double>();
//range Min
	if (!_sdf->HasElement("rangeMin"))
  {
    ROS_DEBUG("Missing paramete <rangeMin> in GazeboRosTexel, defaults to 0");
    this->rangeMin_= 0;
  }
  else
    this->rangeMin_ = _sdf->GetElement("rangeMin")->Get<double>();
//saturation max
	if (!_sdf->HasElement("satMax"))
  {
    ROS_DEBUG("Missing paramete <satMax> in GazeboRosTexel, defaults to 2^16");
    this->satMax_= 65536;
  }
  else
    this->satMax_ = _sdf->GetElement("satMax")->Get<double>();
//saturation Min
	if (!_sdf->HasElement("satMin"))
  {
    ROS_DEBUG("Missing paramete <satMin> in GazeboRosTexel, defaults to 0");
    this->satMin_= 0;
  }
  else
    this->satMin_ = _sdf->GetElement("satMin")->Get<double>();
//cut frequency
	if (!_sdf->HasElement("cutFrequency"))
  {
    ROS_DEBUG("Missing paramete <cutFrequency> in GazeboRosTexel, defaults to 2");
    this->cutFrequency_= 2;
  }
  else
    this->cutFrequency_ = _sdf->GetElement("cutFrequency")->Get<double>();
    //tactile_rate
	if (!_sdf->HasElement("tactile_rate"))
  {
    ROS_DEBUG("Missing paramete <tactile_rate> in GazeboRosTexel, defaults to 100");
    this->tactile_rate_= 100;
  }
  else
    this->tactile_rate_ = _sdf->GetElement("tactile_rate")->Get<double>();
//sensibility	
	if (!_sdf->HasElement("orderFilter"))
  {
    ROS_DEBUG("Missing paramete <orderFilter> in GazeboRosTexel, defaults to 2");
    this->orderFilter_= 2;
  }
  else
    this->orderFilter_ = _sdf->GetElement("orderFilter")->Get<double>();
    
//robot_namespace_
	if (!_sdf->HasElement("robotNamespace"))
  {
    ROS_DEBUG("Missing paramete <robotNamespace> in GazeboRosTexel, defaults to """);
    this->robot_namespace_= "";
  }
  else
    this->robot_namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>() + "/";
    
   //correlation
	if (!_sdf->HasElement("sigmax"))
  {
    ROS_DEBUG("Missing paramete <sigmax> in GazeboRosTactile, defaults to 1");
    this->sigmax_= 1;
  }
  else
    this->sigmax_ = _sdf->GetElement("sigmax")->Get<double>();
    
	if (!_sdf->HasElement("ksizex"))
  {
    ROS_DEBUG("Missing paramete <ksizex> in GazeboRosTactile, defaults to 3");
    this->ksizex_= 3;
  }
  else
    this->ksizex_ = _sdf->GetElement("ksizex")->Get<double>();
	if (!_sdf->HasElement("sigmay"))
  {
    ROS_DEBUG("Missing paramete <sigmay> in GazeboRosTactile, defaults to 1");
    this->sigmay_= 1;
  }
  else
    this->sigmay_ = _sdf->GetElement("sigmay")->Get<double>();
    
	if (!_sdf->HasElement("ksizey"))
  {
    ROS_DEBUG("Missing paramete <ksizey> in GazeboRosTactile, defaults to 3");
    this->ksizey_= 3;
  }
  else
    this->ksizey_ = _sdf->GetElement("ksizey")->Get<double>();



	if (!_sdf->HasElement("rows"))
	{
		  // if parameter tag does NOT exist
		  ROS_ERROR("Missing parameter <rows> in GazeboRosTexel");
	}
		  // if parameter tag exists, get its value
	else this->rows_= _sdf->GetElement("rows")->Get<unsigned int>();
	
		if (!_sdf->HasElement("columns"))
	{
		  // if parameter tag does NOT exist
		  ROS_ERROR("Missing parameter <columns> in GazeboRosTexel");
	}
		  // if parameter tag exists, get its value
	else this->columns_ = _sdf->GetElement("columns")->Get<unsigned int>();
	
	
  /// set Curve of calibration
	if (!_sdf->HasElement("liniaritySensibility"))
  {
    ROS_DEBUG("Missing parameter <liniarySensibility> in GazeboRosTexel, defaults to 1");
    this->liniaritySensibility_= 1;
  }
  else
    this->liniaritySensibility_= _sdf->GetElement("liniaritySensibility")->Get<double>();
    
   //correlation
	if (!_sdf->HasElement("liniarityOfset"))
  {
    ROS_DEBUG("Missing parameter <liniarityOfset> in GazeboRosTactile, defaults to 0");
    this->liniarityOfset_= 0;
  }
  else
    this->liniarityOfset_ = _sdf->GetElement("liniarityOfset")->Get<double>();
	
	std::string texelTopicName;
	std::string imageTopicName;
	
		if (!_sdf->HasElement("imageTopicName"))
	{
		  // if parameter tag does NOT exist
		  ROS_DEBUG("Missing parameter <imageTopicName> in GazeboRosTexel, default to Texel");
		  imageTopicName = "ImageTactile";
	}
		  // if parameter tag exists, get its value
	else imageTopicName = _sdf->GetElement("imageTopicName")->Get<std::string>();
	
		if (!_sdf->HasElement("dataTopicName"))
	{
		  // if parameter tag does NOT exist
		  ROS_DEBUG("Missing parameter <DataTopicName> in GazeboRosTexel, default to Texel");
		  texelTopicName = "DataTactile";
	}
		  // if parameter tag exists, get its value
	else texelTopicName = _sdf->GetElement("dataTopicName")->Get<std::string>();
	
	// Get world name.
  this->worldName_ = this->parentSensor_->GetWorldName();

  // Get name of parent sensor.
  this->parentSensorName_ = this->parentSensor_->GetName();
	
  // Get parent model name scooped
	std::string parentName = this->parentSensor_->GetParentName();	
	this->sample_rate_ = this->parentSensor_->GetUpdateRate ();	
		
  //////////////////name for ROS topic's	///////////////////////	

  this->ModelName_ = parentName;
	boost::replace_all(this->ModelName_, "::", "/");
	this->texel_topic_name_ = "/" + this->ModelName_  + "/" + texelTopicName;
  this->image_sensor_topic_name_ = "/" + this->ModelName_  + "/" + imageTopicName + "sensor";
  this->image_force_topic_name_ = "/" + this->ModelName_  + "/" + imageTopicName + "force";
  

}


//////////////////////////////////////////////////////////////////
//						creation du collisionNamesToArea 									//
//////////////////////////////////////////////////////////////////

void GazeboRosSensor::GetCollisionNamesToArea()
{
	// Get collision names of parent sensor and physics pointers.
  physics::WorldPtr world = physics::get_world(this->worldName_);
  
  unsigned int collisionCount = this->parentSensor_->GetCollisionCount();
  this->collisionCount_ = collisionCount;
  for (unsigned int i = 0; i < collisionCount; ++i)
  {
    std::string collisionScopedName = this->parentSensor_->GetCollisionName(i);
    // Strip off ::collision_name to get link name
    std::string linkName = collisionScopedName.substr(0,
                           collisionScopedName.rfind("::"));
    // Get unscoped name of collision
    std::string collisionName =
      collisionScopedName.substr(collisionScopedName.rfind("::") + 2);
    // Get physics pointers
    physics::EntityPtr entity = world->GetEntity(linkName);
    if (entity && entity->HasType(physics::Base::LINK))
    {
      physics::LinkPtr link =
        boost::dynamic_pointer_cast<physics::Link>(entity);
      if (link)
      {
        physics::CollisionPtr collision = link->GetCollision(collisionName);
        if (collision)
        {
          physics::ShapePtr shape = collision->GetShape();
          if (shape->HasType(physics::Base::BOX_SHAPE))
          {
            physics::BoxShapePtr box =
              boost::dynamic_pointer_cast<physics::BoxShape>(shape);
            if (box)
            {
              math::Vector3 size = box->GetSize();
              std::vector<double> sizeVector;
              sizeVector.push_back(size.x);
              sizeVector.push_back(size.y);
              sizeVector.push_back(size.z);
              //we take the two longer sides to calculate the area
              std::sort(sizeVector.begin(), sizeVector.end());
              double area = sizeVector[1] * sizeVector[2];
              if (area > 0.0)
                this->collisionNamesToArea_[collisionScopedName] = area;
            }
          }
        }
      }
    }
  } 
}



// Put tactile data to the interface
void GazeboRosSensor::ContactQueueThread()
{
  static const double timeout = 0.01;

  while (this->rosnode_->ok())
  {
    this->contact_queue_.callAvailable(ros::WallDuration(timeout));
  }
}


