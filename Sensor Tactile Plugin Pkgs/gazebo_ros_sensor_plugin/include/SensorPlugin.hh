#ifndef _TEXEL_PLUGIN_HH_
#define _TEXEL_PLUGIN_HH_

#include <string>
#include <vector>
#include <stdio.h>

#include <sdf/sdf.hh> 
#include <sdf/Param.hh>
/// These headers of Gazebo
#include <gazebo/gazebo.hh>
#include <gazebo/physics/Base.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/sensors/sensors.hh> 
/// These headers will allow us to convert to ros type image
#include <sensor_msgs/Image.h>
#include <std_msgs/Header.h>
#include <std_msgs/String.h>
/// These headers of ROS
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>
/// These headers of my custom msg
#include <gazebo_ros_sensor_plugin/TexelData.h> 
#include <gazebo_ros_sensor_plugin/SensorData.h>
/// These headers of the filter
#include <DspFilters/Dsp.h>
/// These headers of openCv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>


namespace gazebo
{
  /// \brief An example plugin for a contact sensor.
  class GazeboRosSensor : public SensorPlugin
  {
  // \brief Constructor.
    public: GazeboRosSensor();
    /// \brief Destructor.
    public: virtual ~GazeboRosSensor();
    /// \brief Load the sensor plugin.
    public: virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);
    /// \brief Callback that recieves the contact sensor's update signal.
    private: virtual void OnUpdate();
    private: void ContactQueueThread(); 
	private: void initSensor();
   	private: void loadParameters(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);
   	private: void GetCollisionNamesToArea();
   	private: void updateSensorMsg();
   	private: void convoluteSensorMsg();
	private: void updateImageForce();
	private: void updateImageSensor();
	private: void updateImageSensorBis();

    /// \brief Pointer to the contact sensor
    private: sensors::ContactSensorPtr parentSensor_;
    /// updated signal and the OnUpdate callback.
    private: event::ConnectionPtr updateConnection_;

    /// \brief pointer to ros node  
    private: ros::NodeHandle* rosnode_;
    private: ros::Publisher contact_pub_;
    private: ros::Publisher image_force_pub_;
    private: ros::Publisher image_sensor_pub_;

 	/// \brief the messages
    private: gazebo_ros_sensor_plugin::SensorData sensorMsg_;
    private: sensor_msgs::Image sensorImage_;
    private: sensor_msgs::Image forceImage_;
    private: ros::CallbackQueue contact_queue_;
    
    /// \brief set topic name
    private: std::string texel_topic_name_;
    private: std::string image_force_topic_name_;
    private: std::string image_sensor_topic_name_;

    /// \brief for setting ROS name space
    private: std::string robot_namespace_;
    /// \brief World name
    private: std::string worldName_;
    /// \brief Parent sensor name
    private: std::string parentSensorName_;
    /// \brief ModelN name
    private: std::string ModelName_;
    
    /// \brief set Range
    private: double rangeMax_;
    private: double rangeMin_;
    /// \brief set saturation
    private: double satMax_;
    private: double satMin_;
    /// \brief set filter
    private: std::vector<Dsp::Filter*> fi_;
    private: double cutFrequency_;
    private: double sample_rate_;
    private: double orderFilter_;
    private: double tactile_rate_;
    private: std::vector<double> bufferForce_;
        
    /// \brief set Correlation
    private: int ksizex_;
    private: int ksizey_;
    private: double sigmax_;
    private: double sigmay_;
    /// \brief set Curve of calibration
    private: double liniaritySensibility_;
    private: double liniarityOfset_;
    /// \brief set rows
    private: unsigned int rows_;
    /// \brief set columns
    private: unsigned int columns_; 
    private: int collisionCount_; 
	/// \brief count the number of iterations
	private: unsigned int seq_;
	/// \brief counter for publish
	private: unsigned int hz_count_;
 	/// \brief Parent sensor collision names.
    private: boost::unordered_map<std::string, double> collisionNamesToArea_;
    private: boost::thread callback_queue_thread_;
    
  };
}
#endif
